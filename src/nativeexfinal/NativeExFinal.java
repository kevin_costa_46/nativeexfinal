package nativeexfinal;

import java.util.Scanner;
import org.exist.xmldb.XQueryService;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.CompiledExpression;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;

/**
 *
 * @author Kevin
 */
public class NativeExFinal {

    protected static String DRIVER = "org.exist.xmldb.DatabaseImpl";
    protected static String collectionPath = "/db/";
    protected static String resourceName = "mondial.xml";
    protected static Class cl;
    protected static Database database;
    protected static Collection col;
    protected static XQueryService xqs;
    protected static CompiledExpression compiled;
    protected static ResourceSet result;
    protected static ResourceIterator i;
    protected static Resource res;
    private static final String USER = "admin";
    private static final String PASS = "patata";
    private static Scanner lector;
    private static String URI = "xmldb:exist://172.24.7.208:8080/exist/xmlrpc";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Instància del driver i de la base de dades
        try {
            cl = Class.forName(DRIVER);
            database = (Database) cl.newInstance();
            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(URI + collectionPath, USER,
                    PASS);
        } catch (XMLDBException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        int opcio;
        lector = new Scanner(System.in);

        do {
            opcio = menu();

            try {
                switch (opcio) {
                    case 1:
                        llistarPaisosParametres();
                        break;
                    case 2:
                        updateEtiquetes();
                        break;
                    case 3:
                        afegirEtiqueta();
                        break;
                    case 4:
                        // No està fet
                        break;
                    case 5:
                        esborrarEtiqueta();
                        break;
                }
            } catch (XMLDBException e) {
                System.out
                        .println("Ha ocurregut un error amb les dades entrades");
            }
        } while (opcio != 0);
    }

    private static int menu() {
        int opcio;
        System.out
                .print("***********************************************************************\n"
                        + "*                         Menú principal                              *\n"
                        + "*                                                                     *\n"
                        + "*  1. Llistar països ordenadament segons qualsevol dels etiquetas     *\n"
                        + "*  2. Actualitzar un valor determinat, per un país determinat         *\n"
                        + "*  3. Afegir un nou etiqueta a un país determinat                     *\n"
                        + "*  4. Modificació remota                                              *\n"
                        + "*  5. Esborrar una informació d’un país determinat                    *\n"
                        + "*  0. Sortir                                                          *\n"
                        + "***********************************************************************\n");
        do {
            opcio = Integer.parseInt(lector.nextLine());
        } while (opcio > 5 || opcio < 0);
        return opcio;
    }

    /**
     * Mètode que llista els països ordenats ascendentment o descendentment (com
     * es vulgui) per qualsevol etiqueta
     *
     * @throws XMLDBException
     */
    private static void llistarPaisosParametres() throws XMLDBException {

        System.out
                .println("Escriu el nom del tag que vols ordenar els països (name o population)");
        String opcio = lector.nextLine();
        System.out
                .println("Escriu si vols que s'ordeni (ascending o descending)");
        String ordenacio = lector.nextLine();

        String xQuery = "for $x in doc('" + resourceName
                + "')//country order by $x/" + opcio + " " + ordenacio
                + " return (data($x/name), data($x/" + opcio + "))";

        XQueryService service;

        service = (XQueryService) col.getService("XQueryService", "1.0");
        service.setProperty("indent", "yes");

        ResourceSet result = service.query(xQuery);
        ResourceIterator i = result.getIterator();
        while (i.hasMoreResources()) {
            Resource r = i.nextResource();
            System.out.println((String) r.getContent());
        }
    }

    /**
     * Mètode que modifica el valor d'una etiqueta que es vulgui d'un país que
     * es vulgui
     *
     * @throws XMLDBException
     */
    private static void updateEtiquetes() throws XMLDBException {
        System.out.println("Escriu el nom del país: ");
        String pais = lector.nextLine();
        System.out.println("Escriu l'etiqueta (name o population)");
        String etiqueta = lector.nextLine();
        System.out.println("Escriu el nou valor");
        String valor = lector.nextLine();

        String xQuery = "update value //country[name='" + pais + "']/"
                + etiqueta + " with '" + valor + "'";

        XQueryService service;

        service = (XQueryService) col.getService("XQueryService", "1.0");
        service.setProperty("indent", "yes");

        service.query(xQuery);

        if (etiqueta.equals("name")) {
            xQuery = "//country[name= '" + valor + "']";
        } else {
            xQuery = "//country[name= '" + pais + "']";
        }

        service = (XQueryService) col.getService("XQueryService", "1.0");
        service.setProperty("indent", "yes");

        ResourceSet result = service.query(xQuery);
        ResourceIterator i = result.getIterator();
        while (i.hasMoreResources()) {
            Resource r = i.nextResource();
            System.out.println((String) r.getContent());
        }
    }

    /**
     * Mètode que afegeix una etiqueta a un país que l'usuari vol
     *
     * @throws XMLDBException
     */
    private static void afegirEtiqueta() throws XMLDBException {
        System.out.println("Escriu el nom del país: ");
        String pais = lector.nextLine();
        System.out.println("Escriu el nom de l'etiqueta que vols afegir");
        String etiqueta = lector.nextLine();
        System.out.println("Escriu el valor de l'etiqueta");
        String valor = lector.nextLine();

        String xQuery = "update insert <" + etiqueta + ">" + valor + "</"
                + etiqueta + "> into //country[name='" + pais + "']";

        XQueryService service;

        service = (XQueryService) col.getService("XQueryService", "1.0");
        service.setProperty("indent", "yes");

        service.query(xQuery);

        xQuery = "//country[name= '" + pais + "']";

        service = (XQueryService) col.getService("XQueryService", "1.0");
        service.setProperty("indent", "yes");

        ResourceSet result = service.query(xQuery);
        ResourceIterator i = result.getIterator();
        while (i.hasMoreResources()) {
            Resource r = i.nextResource();
            System.out.println((String) r.getContent());
        }
    }

    /**
     * Mètode que esborra una etiqueta que vol l'usuari de un país
     *
     * @throws XMLDBException
     */
    private static void esborrarEtiqueta() throws XMLDBException {
        System.out.println("Escriu el nom del país: ");
        String pais = lector.nextLine();
        System.out.println("Escriu el nom de l'etiqueta que vols esborrar");
        String etiqueta = lector.nextLine();

        String xQuery = "update delete //country[name='" + pais + "']/"
                + etiqueta;

        XQueryService service;
        service = (XQueryService) col.getService("XQueryService", "1.0");
        service.setProperty("indent", "yes");

        service.query(xQuery);

        xQuery = "//country[name= '" + pais + "']";

        service = (XQueryService) col.getService("XQueryService", "1.0");
        service.setProperty("indent", "yes");

        ResourceSet result = service.query(xQuery);
        ResourceIterator i = result.getIterator();
        while (i.hasMoreResources()) {
            Resource r = i.nextResource();
            System.out.println((String) r.getContent());
        }
    }
}